package io.github.tubakyle.uniquekills;

import io.github.tubakyle.kpslib.ConfigAccessor;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kyle Sferrazza on 8/7/2014.
 * Created by Kyle Sferrazza on 8/7/2014.
 * This file is a part of: UniqueKills.
 * All rights reserved.
 */
public class DeathHandler implements Listener {

    private final UniqueKills plugin;
    private final ConfigAccessor killsConfig;

    public DeathHandler (UniqueKills plugin, ConfigAccessor killsConfig) {
        this.plugin = plugin;
        this.killsConfig = killsConfig;
    }

    @EventHandler
    public void onKill (PlayerDeathEvent event) {
        killsConfig.reloadConfig();
        Player victim = event.getEntity();
        Player killer = victim.getKiller();
        try {
            if (killer.equals(victim)) {
                return;
            }
            if (!(killer instanceof Player)) {
                return;
            }
        } catch (NullPointerException npe) {
            return;
        }
        if (killsConfig.getConfig().isSet(killer.getUniqueId().toString())) {
            //If the killer has a list of victims,
            List<String> playersKilled = killsConfig.getConfig().getStringList(killer.getUniqueId().toString());
            if (playersKilled.contains(victim.getUniqueId().toString())) {
                //Killer has already killed victim, -- do nothing.
                return;
            } else {
                //Killer has not yet killed victim
                int streak = playersKilled.size();
                streak++;

                runRewards(killer, victim, streak);

                //add player to the killed list
                playersKilled.add(victim.getUniqueId().toString());

                //set config to new killed list
                killsConfig.getConfig().set(killer.getUniqueId().toString(), playersKilled);
                killsConfig.saveConfig();
            }
        } else {
            //Create new list.
            List<String> killsList = new ArrayList<String>(1);
            killsList.add(victim.getUniqueId().toString());
            killsConfig.getConfig().set(killer.getUniqueId().toString(), killsList);
            killsConfig.saveConfig();

            runRewards(killer, victim, 1);
        }
    }

    public void runRewards(Player killer, Player victim, int streak) {
        if (plugin.getConfig().isSet("rewards.all")) {
            //If 'all' rewards are set
            for (String cmd : plugin.getConfig().getStringList("rewards.all")) {
                //For each command in all, run it
                runCmd(killer, victim, streak, cmd);
            }
        }
        if (plugin.getConfig().isSet("rewards." + streak)) {
            //if rewards are set for current streak,
            for (String cmd : plugin.getConfig().getStringList("rewards." + streak)) {
                //for each command, run it
                runCmd(killer, victim, streak, cmd);
            }
        }
    }

    public void runCmd(Player killer, Player victim, int streak, String cmd) {
        //replace all vars and chatcolor codes
        cmd = cmd.replace("%k", killer.getName());
        cmd = cmd.replace("%v", victim.getName());
        cmd = cmd.replace("%s", Integer.toString(streak));
        cmd = ChatColor.translateAlternateColorCodes('&', cmd);
        CommandSender console = plugin.getServer().getConsoleSender();
        Bukkit.dispatchCommand(console, cmd);
    }
}
