package io.github.tubakyle.uniquekills;

import io.github.tubakyle.kpslib.ConfigAccessor;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Kyle Sferrazza on 8/7/2014.
 * This file is a part of: UniqueKills.
 * All rights reserved.
 */

    //TODO: CREATE BUKKIT PROJECT - add repo, issue tracker

public class UniqueKills extends JavaPlugin {
    @Override
    public void onEnable() {
        ConfigAccessor killsAccessor = new ConfigAccessor(this, "kills.yml");
        saveDefaultConfig();
        killsAccessor.saveDefaultConfig();
        DeathHandler deathHandler = new DeathHandler(this, killsAccessor);
        getServer().getPluginManager().registerEvents(deathHandler, this);
        UKCommands ukCommands = new UKCommands(this, killsAccessor);
        getCommand("ukills").setExecutor(ukCommands);

    }
}
