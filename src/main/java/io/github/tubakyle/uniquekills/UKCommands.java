package io.github.tubakyle.uniquekills;

import io.github.tubakyle.kpslib.ConfigAccessor;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * Created by Kyle Sferrazza on 8/8/2014.
 * This file is a part of: UniqueKills.
 * All rights reserved.
 */
public class UKCommands implements CommandExecutor {

    private final UniqueKills plugin;
    private final ConfigAccessor killsConfig;

    public UKCommands (UniqueKills plugin, ConfigAccessor killsConfig) {
        this.plugin = plugin;
        this.killsConfig = killsConfig;
    }
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (commandSender instanceof Player && command.getName().equalsIgnoreCase("ukills") && commandSender.hasPermission("uniquekills.ukills")) {
            Player killer = ((Player) commandSender);
            String msg = plugin.getConfig().getString("ukills-message");
            int streak = 0;
            killsConfig.reloadConfig();
            if (killsConfig.getConfig().isSet(killer.getUniqueId().toString())) {
                //If the killer has a list of victims,
                List<String> playersKilled = killsConfig.getConfig().getStringList(killer.getUniqueId().toString());
                streak = playersKilled.size();
            }
            msg = msg.replace("%s", Integer.toString(streak));
            msg = ChatColor.translateAlternateColorCodes('&', msg);
            killer.sendMessage(msg);
        }
        return true;
    }
}
