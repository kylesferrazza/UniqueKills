# UniqueKills
This plugin causes console commands to be run when a player reaches a certain number of 'unique' kills. (unique meaning on different players)

## Commands
<pre>
/ukills
</pre>
is the only command. It will tell the player how many unique kills they have. 
<br>The message it sends is configurable in config.yml under 'ukills-message'.

## Permissions
<pre>
uniquekills.ukills
</pre>
allows the player to use the command
<pre>
/ukills
</pre>

## Config
### kills.yml does not need to be edited! It is used by the plugin to store the unique kills a player has.
### If you wish to reset a player's unique kills, just delete kills.yml.
'Rewards' are run as console commands. Use '%k' (killer) to substitute the killer's name, '%v' (victim) to represent the victim's name, and '%s' (streak) to substitute their number of unique kills.
<br>All commands can use color codes and will be run as the console.
<br>Rewards may have an unlimited number of commands, and there may be rewards for any number of kills - just follow the layout in the default config.
<br>Commands under 'all' will run on every unique kill.
<br>Here is the default config for reference:
<pre>
ukills-message: '&6[UniqueKills]&r You have %s unique kills.'
rewards:
  all:
    - 'say Player ''%k'' has %s unique kills! They killed ''%v'' for the first time. Someone give them a &6gold star&r!!'
    - 'give %k 397 1 3 {SkullOwner:%v}'
    - 'tell %v %k killed you for the first time!'
  5:
    - 'give %k diamond 1'
    - 'tell %k &2Nice streak&c! &rNext at &l&650&r&c!'
  50:
    - 'give %k diamond 576'
    - 'say &1%k&r has achieved the impossible! &c50 KILLS!!&r'
</pre>